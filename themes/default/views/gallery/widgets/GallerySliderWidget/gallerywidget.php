<?php

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/gallery-slider-widget.css');
?>

<div class="carousel slide row" data-ride="carousel" data-type="multi" data-interval="2000" id="fruitscarousel">
    <div class="carousel-inner">
        <?php
        $i = 0;
        ?>
        <?php foreach ($model as $item): ?>
            <?php
            $i++;
            ?>
            <div class="item <?= $i == 1 ? 'active' : '' ?>">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <a href="#">
                        <img src="<?= $item->image->getImageUrl(300, 300); ?>" class="img-responsive">
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        jQuery('.carousel[data-type="multi"] .item').each(function () {
            var next = jQuery(this).next();
            if (!next.length) {
                next = jQuery(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo(jQuery(this));

            for (var i = 0; i < 2; i++) {
                next = next.next();
                if (!next.length) {
                    next = jQuery(this).siblings(':first');
                }
                next.children(':first-child').clone().appendTo($(this));
            }
        });
    });
</script>

