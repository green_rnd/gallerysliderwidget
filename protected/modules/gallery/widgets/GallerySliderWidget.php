<?php

Yii::import('application.modules.gallery.models.*');

class GallerySliderWidget extends yupe\widgets\YWidget
{
    // сколько изображений выводить на одной странице
    public $limit = 10;

    // ID-галереи
    public $galleryId;

    // Галерея
    public $gallery;

    public $view = 'gallerywidget';

    /**
     * Запускаем отрисовку виджета
     *
     * @return void
     */
    public function run()
    {
        $dataProvider = new CActiveDataProvider(
            'ImageToGallery', [
                'criteria' => [
                    'condition' => 't.gallery_id = :gallery_id',
                    'params' => [':gallery_id' => $this->galleryId],
                    'limit' => $this->limit,
                    'order' => 't.position',
                    'with' => 'image',
                ],
                'pagination' => ['pageSize' => $this->limit],
            ]
        );

        $model = $dataProvider->getData();

        $this->render(
            $this->view,
            [
                'model' => $model
            ]
        );
    }
}
